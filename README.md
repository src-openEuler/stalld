# stalld

#### 介绍
The stalld program (which stands for 'stall daemon') is a
mechanism to prevent the starvation of operating system threads in a
Linux system. The premise is to start up on a housekeeping cpu (one
that is not used for real-application purposes) and to periodically
monitor the state of each thread in the system, looking for a thread
that has been on a run queue (i.e. ready to run) for a specifed length
of time without being run. This condition is usually hit when the
thread is on the same cpu as a high-priority cpu-intensive task and
therefore is being given no opportunity to run.
When a thread is judged to be starving, stalld changes
that thread to use the SCHED_DEADLINE policy and gives the thread a
small slice of time for that cpu (specified on the command line). The
thread then runs and when that timeslice is used, the thread is then
returned to its original scheduling policy and stalld then
continues to monitor thread states.
There is now an experimental option to boost using SCHED_FIFO. This
logic is used if the running kernel does not support the
SCHED_DEADLINE policy and may be forced by using the -F/--force_fifo
option.




#### 安装教程

Install stalld rpm package:

yum install stalld

#### 使用说明
Logging options

-l/--log_only: only log information (do not boost) [false]
-v/--verbose: print info to the std output [false]
-k/--log_kmsg: print log to the kernel buffer [false]
-s/--log_syslog: print log to syslog [true]


Startup options

-c/--cpu: list of cpus to monitor for stalled threads [all cpus]
-f/--foreground: run in foreground [false but true when -v]
-P/--pidfile: write daemon pid to specified file [no pidfile]


Boosting options

-p/--boost_period: SCHED_DEADLINE period [ns] that the starving task will receive [1000000000]
-r/--boost_runtime: SCHED_DEADLINE runtime [ns] that the starving task will receive [20000]
-d/--boost_duration: how long [s] the starving task will run with SCHED_DEADLINE [3]
-F/--force_fifo: force using SCHED_FIFO for boosting


Monitoring options

-t/--starving_threshold: how long [s] the starving task will wait before being boosted [60]
-A/--aggressive_mode: dispatch one thread per run queue, even when there is no starving
threads on all CPU (uses more CPU/power). [false]

#### 参与贡献

master分支使用最新的上游版本，如果检测到上游有最新版本发布，先形成issue后再提交对应PR更新，流程如下。
1.  提交issue
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
