Name:		stalld
Version:	1.19.7
Release:	1
Summary:	Daemon that finds starving tasks and gives them a temporary boost

License:	GPLv2
URL:		https://gitlab.com/rt-linux-tools/%{name}
Source0:	https://gitlab.com/rt-linux-tools/%{name}/-/archive/%{version}/%{name}-v%{version}.tar.bz2
Patch0:		riscv_support.patch

BuildRequires:	glibc-devel
BuildRequires:	gcc
BuildRequires:	make
BuildRequires:	systemd

Requires:	systemd

%ifnarch i686
BuildRequires:	bpftool
BuildRequires:	clang
BuildRequires:	libbpf-devel
BuildRequires:  llvm
 
Requires:	libbpf
%endif

%description
The stalld program monitors the set of system threads,
looking for threads that are ready-to-run but have not
been given processor time for some threshold period.
When a starving thread is found, it is given a temporary
boost using the SCHED_DEADLINE policy. The default is to
allow 10 microseconds of runtime for 1 second of clock time.

%prep
%autosetup -p1 -n %{name}-v%{version}

%build
%make_build CFLAGS="%{optflags} %{build_cflags} -DVERSION="\\\"%{version}\\\"""  LDFLAGS="%{build_ldflags}"

%install
%make_install DOCDIR=%{_docdir} MANDIR=%{_mandir} BINDIR=%{_bindir} DATADIR=%{_datadir} VERSION=%{version}
%make_install -C systemd UNITDIR=%{_unitdir}
mkdir -p %{buildroot}%{_docdir}/%{name}
mv %{buildroot}%{_docdir}/README.md %{buildroot}%{_docdir}/%{name}/

%files
%{_bindir}/%{name}
%{_bindir}/throttlectl
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/sysconfig/stalld
%doc %{_docdir}/%{name}/README.md
%doc %{_mandir}/man8/stalld.8*
%license gpl-2.0.txt

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%changelog
* Mon Nov 25 2024 tenglei <tenglei@kylinos.cn> - 1.19.7-1
- update version to 1.19.7

* Mon Nov 11 2024 sqfu <dev01203@linx-info.com> - 1.19.6-1
- update version to 1.19.6

* Fri Jul 19 2024 tenglei <tenglei@kylinos.cn> - 1.19.5-1
- update package version to 1.19.5 

* Sat Mar 23 2024 luojun <luojun.oerv@isrc.iscas.ac.cn> - v1.19.1-1
- update to v1.19 and support for oe-riscv

* Thu Sep 14 2023 duyiwei <duyiwei@kylinos.cn> - 1.16-3
- move README

* Thu Jul 20 2023 lvgenggeng <lvgenggeng@uniontech.com> - 1.16-2
- add support for loongarch64

* Mon Nov 07 2022 duyiwei <duyiwei@kylinos.cn> - 1.16-1
- upgrade version to 1.16

* Tue Jun 7 2022 duyiwei <duyiwei@kylinos.cn> - 1.15-1
- upgrade to 1.15

* Mon Jan 24 2022 duyiwei <duyiwei@kylinos.cn> - 1.14.1-1
- Package init
